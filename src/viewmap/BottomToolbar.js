import React, { Component } from 'react';
import { Text,
         View,
         Button,
         StyleSheet,
         Image,
         ImageButton,
         TouchableHighlight,
         TouchableOpacity
         } from 'react-native';
import ViewMap from "./ViewMap";

export default class BottomToolbar extends Component{

    constructor(props) {
        super(props)

        this.state = {
          loading: true,
          top10:[],
         currentuserid:'',
        }
  
    }

    action(){
        alert("ok");
    }

    render() {
        return (

            <View style = {styles.topToolbar} >

                <TouchableOpacity onPress={() =>this.props.fitAllMarkers()}>
                    <Image style={styles.button} source={require('../../assets/img/location.png')} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() =>alert('123')}>
                    <Image style={styles.button} source={require('../../assets/img/fastest.png')} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() =>alert('123')}>
                    <Image style={styles.button} source={require('../../assets/img/info.png')} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() =>alert('123')}>
                    <Image style={styles.button} source={require('../../assets/img/medical.png')} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() =>alert('123')}>
                    <Image style={styles.button} source={require('../../assets/img/dismiss.png')} />
                </TouchableOpacity>
               
            </View>

        )
    }
}

const styles = StyleSheet.create({

    topToolbar:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: "100%",
        height: 60
      },
      button:{
        padding: 10,
        borderRadius: 5,
        width : 50,
        height : 50
      }
    }
)