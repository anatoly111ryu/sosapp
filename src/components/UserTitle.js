import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    ImageButton,
    TouchableHighlight,
    TouchableOpacity,
    Alert,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';

import {
    Menu,
    MenuContext,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    renderers,
} from 'react-native-popup-menu';
import { EvilIcons } from "@expo/vector-icons";
import variables from "../../native-base-theme/variables/commonColor";
import { Button } from "native-base";
// import { Actions } from 'react-native-router-flux';

const { Popover } = renderers

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

export default class UserTitle extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isOpen: false,
            username: '',
            user_image:''
        }

        var ls = require('react-native-local-storage');

        ls.get('first_name').then((username) => {

            this.setState({ 'username': username });
        });

        ls.get('user_image').then((user_image) => {

            this.setState({ 'user_image': user_image });
        });

    

    }
    render() {

        if(this.state.user_image == ""){

            return (

                <View style={styles.saver} >
                    <View style={styles.imageView}>
                        <Image style={styles.image} source={require('../../assets/img/user_male_1.png')} />
                    </View>

                    <View style={styles.textView}>
                        <Text style={styles.userText}>{this.state.username}</Text>
                    </View>

                </View>

            );
        }
        else{

            return (

                <View style={styles.saver} >
                    <View style={styles.imageView}>
                        <Image style={styles.image} source={{uri : this.state.user_image}} />
                    </View>

                    <View style={styles.textView}>
                        <Text style={styles.userText}>{this.state.username}</Text>
                    </View>

                </View>

            );


        }
        
    }
}


const styles = StyleSheet.create({
    saver: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',

    },
    imageView: {
        marginTop: 10,
        width: '30%',
        alignItems: 'center',
    },
    image:{
        width: width/8,
        height: width/8 
    },
    textView: {
        marginTop: 10,
        width: '70%',
    },
    userText: {
        fontSize: height / 25,
        fontWeight: '300',
    },
}
)