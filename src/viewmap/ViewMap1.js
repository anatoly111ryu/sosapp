import React from 'react';
import {Platform, 
  ScrollView, 
  Dimensions,
   View, 
   Image, 
   Text, 
   TouchableOpacity, 
   StyleSheet,
  Alert,
  TouchableHighlight
} from 'react-native';

import {
  Container,
  Content,
  Header,
  Title,
  Left,
  Icon,
  Right,
  Button
} from "native-base"

import Storage from 'react-native-key-value-store';
import registerForPushNotificationsAsync from '../Utils/registerForPushNotificationsAsync';
import { Notifications } from 'expo';
import * as firebase from 'firebase';


import MapView from 'react-native-maps';
import Polyline from '@mapbox/polyline';
import Modal from 'react-native-modal'
import api from '../Utils/api';
import UserTitle from '../components/UserTitle'; 
const DEFAULT_PADDING = { top: 40, right: 40, bottom: 40, left: 40 };
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

const SPACE = 0.01;

function createMarker( LATITUDE = 42.032923,LONGITUDE = 19.2,username = "RESCUER",user_id){
  
  return {
    latitude: Number(LATITUDE) ,
    longitude: Number(LONGITUDE) ,
    username : username,
    user_id: user_id
  }

}

class StaticMap extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isInformationModalVisible: false,
      isReportUserModalVisible: false,
      origin:{
          latitude:'',
          longitude:''

      },
      coords :[],
      destination:{
          latitude:'',
          longitude:''
      },
      sos_signal : {},
      markers    : [],
      user_id :'' ,
      
      username :   '' ,
      modalVisible: false,
      title : "",
      body : "",      
      
    }

    this.mapView = null;

 }

  componentWillMount(){

    var ls = require('react-native-local-storage');
    ls.get('is_athenticated').then((is_athenticated) => {
        if (!is_athenticated){
          
          alert("Please log in");
          this.props.navigation.navigate('SignIn')
        }
        else{
          ls.get('first_name').then((first_name) =>{

          })
          
          setTimeout(() => this.fitAllMarkers(), 2000);
        }
    });

  }

  async storageClear()
  {
    await Storage.clear();
  }

  componentDidMount() {
  
    this.storageClear();

    this._notificationSubscription = this._registerForPushNotifications();

    var ls = require('react-native-local-storage');
    
    ls.get('user_id').then((user_id) =>{

      this.setState({user_id:user_id});
        let data = {
          'userid' : user_id
        }
      api.postApi('sos_mobile.php', data)
      .then((res) => {

          if(res.status == '1'){         
         
              res.users.map((mark,index) => {
                this.state.markers.push(createMarker(mark.latitude, mark.longitude,mark.first_name,mark.user_id));
              })
                navigator.geolocation.getCurrentPosition(
                  (position) => {
                    const initialPosition = JSON.stringify(position);
                    this.setState({ initialPosition });
                    let my_position = JSON.parse(initialPosition); 
                    let my_info = createMarker(my_position.coords.latitude,my_position.coords.longitude,"ME",this.state.user_id);
                    this.setState({origin: my_info});
                    this.setState({destination: my_info});
                    this.state.markers.push(this.state.origin);

                  },
                  (error) => alert(error.message),
                  { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
                );
              this.setState({markers:
                    this.state.markers                
              })

          } else {
              alert(res.msg);
          }
      })
      .catch((e) => {
        console.error(e);
          alert('Error1')
      }) 

    });   
  }

  toggleModal(visible) {
    this.setState({ modalVisible: visible })
  }  

  fitAllMarkers() {

    this.map.fitToCoordinates(this.state.markers, {
      edgePadding:{ top: 50, right: 50, bottom: 200, left: 50 },
      animated: true

    });
    
  }
  
  isBlock(){
    Alert.alert(
        '',
        'Are you sure want to block the user',
        [
          {text: 'OK', onPress: () =>{ 

            let data = {
              'userid' : this.state.select_user
            }
        
            api.postApi('block_mobile.php', data)
            .then((res) => {
        
                if(res.status == '1'){       
                  alert(res.msg);
                } else {
                    alert(res.msg);
                }
            })
            .catch((e) => {
              console.error(e);
                alert('Error1')
            })
 
        
          }},
          {text: 'No', onPress: () => console.log('No ') ,style: 'cancel' },
        ],
        { cancelable: false }
    )
  }

  signalLocation(){

    this.setState({marks:this.state.marks});
    setTimeout(() => this.fitAllMarkers(), 400);

   }

  fastestWay(){
    var origin = this.state.origin;
    var destination = this.state.destination;
    if(this.state.destination.latitude == "" || this.state.destination.longitude == "")
    {
      alert("Please select destination!");
      return;
    }

    this.setState([
      origin,destination
    ])

    setTimeout(() => 
    this.map.fitToCoordinates([this.state.origin,this.state.destination], {
      edgePadding:{ top: 50, right: 50, bottom: 200, left: 50 },
      animated: true
    })    
    , 400);
    let coords = this.getDirections(origin,destination);

  }

  async getDirections(startLoc, destinationLoc) {

    try {

        let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc.latitude + "," + startLoc.longitude }&destination=${ destinationLoc.latitude + "," + destinationLoc.longitude }`)
        let respJson = await resp.json();
        let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
        let coords = points.map((point, index) => {
            return  {
                latitude : point[0],
                longitude : point[1]
            }
        })

        this.setState({coords: coords})
        return coords
    } catch(error) {
        alert(error)
        return error
    }
  }

  showInformationModal = () => {



    var user_id = this.state.select_user;
    let data = {

      'userid' : user_id

    }

    api.postApi('userInfo_mobile.php', data)
    .then((res) => {

        if(res.status == '1'){  

          var signal_info = res.data[0].comment;
          this.setState({signal_info : res.data[0].comment});


        } else {
            alert(res.msg);
        }
    })
    .catch((e) => {
      console.error(e);
        alert('Error1')
    })

    this.setState({ isInformationModalVisible: true })

 }

  hideInformationModal = () => this.setState({ isInformationModalVisible: false })

  showReportModal = () => this.setState({ isReportModalVisible: true })
  
  hideReportModal = () => this.setState({ isReportModalVisible: false })

  showHospital(){

  }

  dismiss(){


  }

  selectMarker(destination ,user_id){


    this.setState({destination:destination.destination});
    this.setState({select_user: user_id});
 
  }  

  account(){

    var user_id = this.state.select_user;
    let data = {

      'userid' : user_id

    }

    api.postApi('userInfo_mobile.php', data)
    .then((res) => {

        if(res.status == '1'){  

          var user_info = res.data[0];

          Alert.alert(
            "Name :" +  user_info.first_name + "  "  + user_info.last_name,
            'Mobile  :' + user_info.mobile ,
            [
              {text: 'Yes', onPress: () =>{ 

              }}
             
            ],
            { cancelable: false }
        )         
          

        } else {
            alert(res.msg);
        }
    })
    .catch((e) => {
      console.error(e);
        alert('Error1')
    })    
  }

  onMapPress = (e) => {
    if (this.state.coordinates.length == 2) {
      this.setState({
        coordinates: [
          e.nativeEvent.coordinate,
        ],
      });
    } else {
      this.setState({
        coordinates: [
          ...this.state.coordinates,
          e.nativeEvent.coordinate,
        ],
      });
    }
  }

  render() {
      const navigate = this.props.navigation.navigate

      if(Platform.OS === 'ios'){

          return (
          <View>
            <View style = {styles.body}>
                  <Header style={{flexDirection : 'row',  alignItems: 'center',  backgroundColor: 'rgb(250,0,0)',  width : '100%', height : height/6} } >
                  <Left>
                  </Left>
                  <TouchableOpacity  style={styles.banner} onPress = {() => navigate('Home')}>
                      <Image  style={{ marginLeft: 10,width: width/5, height: height/7,resizeMode: 'contain',padding: 5, }} 
                              source={require('../../assets/img/logo.png')} />
                    
                    <Text style={{ marginTop: 10,  marginLeft: 5,  fontSize: width/8,  fontWeight: '400', color: '#fff', }}>RESCUER</Text>
      
                      <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")} >
                          <Icon name="md-menu" style={{fontSize: 50, color: 'black'}}/>
                        </Button>
            
                  </TouchableOpacity>
                  </Header>      
                    <UserTitle  username = {this.state.username}/> 
                  <View style={styles.container}>
                    
                    <ScrollView
                      style={StyleSheet.absoluteFill}
                      contentContainerStyle={styles.scrollview}
                      >
                      <MapView
                        ref={ref => { this.map = ref; }}
                    
                        provider = {this.props.provider}
                        style={styles.map}
                        scrollEnabled={true}
                        zoomEnabled={true}
                        pitchEnabled={false}
                        rotateEnabled={false}
                
                        mapType = 'hybrid'
                        maxZoomLevel = {20}
                        loadingEnabled = {true}
                        showsUserLocation={ true }
                        >
                      <MapView.Polyline 
                          coordinates={this.state.coords}
                          strokeWidth={3}
                          strokeColor="yellow"/>
            
                        {this.state.markers.map((marker, i) => (             
                          <MapView.Marker
                            key={i}
                            coordinate={marker}
                            onSelect = {() => {   
                              this.selectMarker({destination : marker},marker.user_id);            
                              this.setState({username : marker.username});
                                                                                
                            }}                
                          /> 
                        ))}
                      </MapView>
            
                    </ScrollView>
                    <View style ={styles.top}>  
            
                    <View style = {styles.topToolbar} >
                      
            
                      <TouchableOpacity onPress={() =>alert(this.state.username)}>
                          <Image style={styles.button} source={require('../../assets/img/user.png')} />
                          
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.account()}>
                          <Text style = {styles.username}> {this.state.username} </Text>
                          <Image style={styles.button} source={require('../../assets/img/info.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.isBlock()}>
                          <Image style={styles.button} source={require('../../assets/img/block.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.showReportModal()}>
                          <Image style={styles.button} source={require('../../assets/img/report.png')} />
                      </TouchableOpacity>
                          
                      </View> 
                    </View>      
            
                    <View style = {styles.bottomToolbar} >
            
                      <TouchableOpacity onPress={() =>{this.signalLocation()}}>
                          <Image style={styles.button} source={require('../../assets/img/location.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() => this.fastestWay()}>
                          <Image style={styles.button} source={require('../../assets/img/fastest.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.showInformationModal()}>
                          <Image style={styles.button} source={require('../../assets/img/info.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.showHospital()}>
                          <Image style={styles.button} source={require('../../assets/img/hospital.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.dismiss()}>
                          <Image style={styles.button} source={require('../../assets/img/dismiss.png')} />
                      </TouchableOpacity>
            
                      <Modal isVisible={this.state.isInformationModalVisible}>
                        <View style={styles.modal}>
                          <Text style = {{fontSize : 25,alignItems:'center'}}>
                              Signal Information     
            
                          </Text>
                          <Text style = {{fontSize : 20,alignItems:'center'}}>
                              {this.state.signal_info}
                          </Text>
            
                          <Text style = {styles.modal_button} onPress={() => { this.hideInformationModal()}}>Close</Text>
                        
            
                        </View>
                      </Modal>
            
                      <Modal isVisible={this.state.isReportModalVisible}>
                        <View style={styles.modal}>
                          <Text style = {{fontSize : 20}}>
                            This is a report
                          </Text>
                          <Text style = {styles.modal_button} onPress={() => { this.hideReportModal()}}>Close!</Text>
            
                        </View>
                      </Modal>
            
                    </View>        
                  </View>
            </View>
            <View style = {styles.container}>
                        
                <Modal 
                    animationType = {"slide"} 
                    transparent = {true}
                    visible = {this.state.modalVisible}
                    onRequestClose = {() => { console.log("Modal has been closed.")}}>
                
                    <View style = {styles.noti_modal}>
                        <View style = {styles.modalView}>
                            <View style = {styles.spaceView}></View>
                            <View style = {styles.imgCircleView}>
                                <Image style = {{width:50,height:50}} source = {require('../../assets/img/red.png')} />
                            </View>
                            <View style = {styles.messageTextView}>
                                <View style = {styles.messageTitleView}>
                                    <Text style = {styles.titleText}>
                                        {this.state.title}
                                    </Text>
                                </View>
                                <View style = {styles.messageBodyView}>
                                    <Text style = {styles.bodyText}>
                                        {this.state.body}
                                    </Text>
                                </View>
                            </View>
                            <View style = {styles.buttonView}>
                                <View style = {styles.imgButtonView}>
                                    
                                    <TouchableHighlight onPress={() => {this.toggleModal(!this.state.modalVisible)}}>
                                        <Image style = {{width:70,height:70}} source = {require('../../assets/img/checkmark.png')} />
                                    </TouchableHighlight>
                                    
                                    <TouchableHighlight onPress={() => {this.toggleModal(!this.state.modalVisible)}}>
                                        <Image style = {{width:60,height:60}} source = {require('../../assets/img/disabled.png')} />
                                    </TouchableHighlight>
                                </View>
                            </View>
                            
                        </View>
                    </View>
                </Modal>               
            </View>   
          </View>   

          
        );        

      }
      else{
 
        return (

          <View>
              <View style = {styles.body}>
                  <Header style={{flexDirection : 'row',  alignItems: 'center',  backgroundColor: 'rgb(250,0,0)',  width : '100%', height : height/6} } >
                      <Left>
                      </Left>
                      <TouchableOpacity  style={styles.banner} onPress = {() => navigate('Home')}>
                          <Image  style={{ marginLeft: 10,width: width/5, height: height/7,resizeMode: 'contain',padding: 5, }} 
                                  source={require('../../assets/img/logo.png')} />
                        
                        <Text style={{ marginTop: 10,  marginLeft: 5,  fontSize: width/8,  fontWeight: '400', color: '#fff', }}>RESCUER</Text>
          
                          <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")} >
                              <Icon name="md-menu" style={{fontSize: 50, color: 'black'}}/>
                            </Button>
                
                    </TouchableOpacity>
                  </Header>      
                  <UserTitle  username = {this.state.username}/> 
                  <View style={styles.container}>
                    
                    <ScrollView
                      style={StyleSheet.absoluteFill}
                      contentContainerStyle={styles.scrollview}
                      >
                      <MapView
                        ref={ref => { this.map = ref; }}
                    
                        provider = {this.props.provider}
                        style={styles.map}
                        scrollEnabled={true}
                        zoomEnabled={true}
                        pitchEnabled={false}
                        rotateEnabled={false}
                
                        mapType = 'roadmap'
                        maxZoomLevel = {20}
                        loadingEnabled = {true}
                        showsUserLocation={ true }
                        >

                        <MapView.Polyline 
                        coordinates={this.state.coords}
                        strokeWidth={3}
                        strokeColor="yellow"/>  

                        {this.state.markers.map((marker, i) => (             
                          <MapView.Marker
                            key={i}
                            coordinate={marker}
                              onPress = {() =>{

                                this.selectMarker({destination : marker},marker.user_id);            
                                this.setState({username : marker.username});

                              }
                            
                            }
                            /> 
                          ))}



                      </MapView>
            
                    </ScrollView>
                    <View style ={styles.top}>  
            
                    <View style = {styles.topToolbar} >
                      
            
                      <TouchableOpacity onPress={() =>alert(this.state.username)}>
                          <Image style={styles.button} source={require('../../assets/img/user.png')} />
                          
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.account()}>
                          <Text style = {styles.username}> {this.state.username} </Text>
                          <Image style={styles.button} source={require('../../assets/img/info.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.isBlock()}>
                          <Image style={styles.button} source={require('../../assets/img/block.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.showReportModal()}>
                          <Image style={styles.button} source={require('../../assets/img/report.png')} />
                      </TouchableOpacity>
                          
                      </View> 
                    </View>      
            
                    <View style = {styles.bottomToolbar} >
            
                      <TouchableOpacity onPress={() =>{this.signalLocation()}}>
                          <Image style={styles.button} source={require('../../assets/img/location.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() => this.fastestWay()}>
                          <Image style={styles.button} source={require('../../assets/img/fastest.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.showInformationModal()}>
                          <Image style={styles.button} source={require('../../assets/img/info.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.showHospital()}>
                          <Image style={styles.button} source={require('../../assets/img/hospital.png')} />
                      </TouchableOpacity>
            
                      <TouchableOpacity onPress={() =>this.dismiss()}>
                          <Image style={styles.button} source={require('../../assets/img/dismiss.png')} />
                      </TouchableOpacity>
            
                      <Modal isVisible={this.state.isInformationModalVisible}>
                        <View style={styles.modal}>
                          <Text style = {{fontSize : 25,alignItems:'center'}}>
                              Signal Information     
            
                          </Text>
                          <Text style = {{fontSize : 20,alignItems:'center'}}>
                              {this.state.signal_info}
                          </Text>
            
                          <Text style = {styles.modal_button} onPress={() => { this.hideInformationModal()}}>Close</Text>
                        
            
                        </View>
                      </Modal>
            
                      <Modal isVisible={this.state.isReportModalVisible}>
                        <View style={styles.modal}>
                          <Text style = {{fontSize : 20}}>
                            This is a report
                          </Text>
                          <Text style = {styles.modal_button} onPress={() => { this.hideReportModal()}}>Close!</Text>
            
                        </View>
                      </Modal>
            
                    </View>        
                  </View>
            </View>
         
        </View>
        
      ); 

      }

  }

  _registerForPushNotifications() {

    registerForPushNotificationsAsync();

    this._notificationSubscription = Notifications.addListener(

      this._handleNotification

    );
  }

  _handleNotification = (notification) => {

    this.state = {
        username :   '' ,
        modalVisible: false,
        title : "",
        body : "",
      }
    
    this.userID = firebase.auth().currentUser.uid;

    this.setState({ title: notification.data.title });
    
    this.setState({ body: notification.data.body });


    this.save(this.state.title,this.state.body);

    firebase.database().ref('users/' + this.userID + '/notifications').push(notification.data);

    this.toggleModal(true);

  };

  async save(title,body)
  {
      
     
        let key = await Storage.get("Recieve");

        if(key)
        {

            await Storage.set('Recieve',key + 1);

        }
        else{

            await Storage.set("Recieve", 1);

        }

        key = 'Recieve' + (key + 1);

        await Storage.set( key  ,body);

  }	
  

}

StaticMap.propTypes = {
  provider: MapView.ProviderPropType,
};



const styles = StyleSheet.create({
  banner: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: 'rgba(250,0,0,1)',
    width : '100%',
    height: height/7

  },
  body:{
    width: '100%',
    height: height
  },

  container: {
  
    flexDirection: 'column',
    flex : 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginTop : 0,
    
  },
  scrollview: {
    alignItems: 'center'

  },
  top :{
    alignItems: 'center',
    width: "100%"

  },
  map: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    width: '100%',
    height: height
  },
  topToolbar:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

    backgroundColor: '#27282277',
    width: "100%",
    height: 60,
   
  },
  username : {
    color : "white",
    
  },

  bottomToolbar:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: "100%",
    height: 60,
    backgroundColor: '#27282277',
    

  },
  button:{
    padding: 10,
    borderRadius: 5,
 
    width : 50,
    height : 50
  },
  modal:{
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modal_button:{
    backgroundColor: 'lightblue',
    padding: 12,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',

  },

    noti_modal: {
        flex: 1,
        alignItems: 'center',
        padding: 100
    },
    modalView: {
        width:300,
    },  
  
    spaceView: {
        width: '100%',
        height: 50
    },
  
    imgCircleView: {
        width: 100,
        height: 100,
        borderRadius: 50,
        backgroundColor: '#11202195',
        alignItems: 'center',
        justifyContent: 'center',
    },
    messageTextView: {
        width: '100%',
        height: 200,
        marginTop: -20,
        borderRadius: 5,
        backgroundColor: '#11202195',
    },
    buttonView: {
        height: 70,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    imgButtonView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: '100%',
        width: '70%',
        marginTop: 2,
        borderRadius: 5,
        backgroundColor: '#11202195',
    }, 
  
    messageTitleView: {
        width: '100%',
        height: 50,
        alignItems: 'center'
    },
    messageBodyView: {
        width: '100%',
        alignItems: 'center'
    },
    titleText: {
        fontSize: 25,
        lineHeight: 40,
        color: 'white',
        fontWeight: '200',
    },

    bodyText: {
        fontSize: 20,
        lineHeight: 40,
        color: 'white',
        fontWeight: '100'
    },  
  
    item: {
      paddingHorizontal: 16,
      paddingVertical: 12,
    },
    itemContainer: {
      backgroundColor: '#fff',
      borderBottomWidth: StyleSheet.hairlineWidth,
      borderBottomColor: '#ddd',
    },
    image: {
      width: width*0.8,
      height: width*0.8,
      alignSelf: 'center',
      marginTop: height/6,
      marginBottom: 20,
      resizeMode: 'contain',
    },
    title: {
      fontSize: 16,
      fontWeight: 'bold',
      color: '#444',
    },
    description: {
      fontSize: 13,
      color: '#999',
    },

  
});

module.exports = StaticMap;
