import React, { Component, NativeModules } from 'react'

import {
    Container,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Button
} from "native-base";
import {
    TouchableHighlight,
    Image,
    ActivityIndicator,
    View,
    Text,
    ToastAndroid,
    TouchableOpacity,
    TextInput,
    StyleSheet,
    Dimensions,
    ScrollView,
    KeyboardAvoidingView,
    Modal
} from 'react-native'

import { StackNavigator } from 'react-navigation'

import Storage from 'react-native-key-value-store';

import api from '../Utils/api';

import helper from '../Utils/helper';

import UserTitle from '../components/UserTitle';

const { width, height } = Dimensions.get('window');


export default class Newmessage extends Component {

    state = {
        inMessage: [
            { 'name': 'Ben', 'content': 'aaaaaaaaaaaaa' }

        ],
        outMessage: [
            { 'name': 'Ben', 'content': 'aaaaaaaaaaaaa' }

        ],
        newMessageButton: {
            backgroundColor: 'green',
            width: '100%',
            padding: 5,
            marginTop: 10,
            borderRadius: 5
        },
        inboxButton: {
            backgroundColor: 'gray',
            width: '100%',
            padding: 5,
            marginTop: 10,
            borderRadius: 5
        },
        sentButton: {
            backgroundColor: 'gray',
            width: '100%',
            padding: 5,
            marginTop: 10,
            borderRadius: 5
        },
        newMessageView: {
            width: '100%',
            height: '100%',
        },
        inboxView: {
            width: '100%',
            height: '100%',
            display: 'none',
        },
        sentView: {
            width: '100%',
            height: '100%',
            display: 'none',
        }

    }

    constructor(props) {
        super(props);

        var ls = require('react-native-local-storage');
        ls.get('is_athenticated').then((is_athenticated) => {

            if (!is_athenticated) {
                alert("Please log in");
                this.props.navigation.navigate('SignIn')
            }

        });

    }

    componentDidMount() {

        this.load();


    }


    newMessage = () => {

        this.setState({
            newMessageButton: {
                backgroundColor: 'green',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            inboxButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            sentButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            newMessageView: {
                width: '100%',
                height: '100%',
            },
            inboxView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
            sentView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
        })
    }

    inbox = () => {

        this.setState({
            newMessageButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            inboxButton: {
                backgroundColor: 'green',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            sentButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            newMessageView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
            inboxView: {
                width: '100%',
                height: '100%',
            },
            sentView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
        })
    }

    sent = () => {


        this.setState({
            newMessageButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            inboxButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            sentButton: {
                backgroundColor: 'green',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            newMessageView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
            inboxView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
            sentView: {
                width: '100%',
                height: '100%',
            },
        })
    }

    fileButton = () => {

        alert('file')

        // var FileTransfer = require('@remobile/react-native-file-transfer');
        // var FilePickerModule = NativeModules.FilePickerModule;

        //       var that = this;
        //       var fileTransfer = new FileTransfer();
        //       FilePickerModule.chooseFile()
        //       .then(function(fileURL){
        //         var options = {};
        //         options.fileKey = 'file';
        //         options.fileName = fileURL.substr(fileURL.lastIndexOf('/')+1);
        //         options.mimeType = 'text/plain';
        //         var headers = {
        //           'X-XSRF-TOKEN':that.state.token
        //         };
        //         options.headers = headers;
        //         var url = "http://sosapp.studiowebdemo.com/ajax/file_mobile" ;
        //         fileTransfer.upload(fileURL, encodeURI(url),(result)=>
        //         {
        //               console.log(result);
        //           }, (error)=>{
        //               console.log(error);
        //           }, options);
        //      })

    }
    cameraButton = () => {
        alert('camera')
    }
    sendButton = () => {

        let user_id;
        var ls = require('react-native-local-storage');

        ls.get('user_id').then((user_id) => {

            let message = this.state.message;

            let data = {

                'userid': user_id,

                'message': message,
            }

            this.save(message);

            api.postApi('message_mobile.php', data)

                .then((res) => {

                    if (res.status == '1') {

                        alert(res.msg);

                    } else {
                        alert(res.msg);
                    }
                })
                .catch((e) => {
                    console.error(e);
                    alert('Error1')
                })


        })

    }
    viewInMessage = (content) => {
        alert(content)
    }
    viewOutMessage = (content) => {
        alert(content)
    }

    handelMessage = (text) => {

        this.setState({ message: text })
    }

    async save(value) {



        let key = await Storage.get("Sent");

        if (key) {

            await Storage.set('Sent', key + 1);

        }
        else {

            await Storage.set("Sent", 1)

        }

        key = 'Sent' + (key + 1)

        await Storage.set(key, value);

        this.state.outMessage.push({ name: key, content: value });

    }

    async load() {


        let key = await Storage.get("Sent");

        while (key > 0) {

            message = await Storage.get('Sent' + key);

            this.state.outMessage.push({ name: 'Sent' + key, content: message });

            key = key - 1;


        }

        key = await Storage.get("Recieve");

        while (key > 0) {

            message = await Storage.get('Recieve' + key);

            this.state.inMessage.push({ name: 'Recieve' + key, content: message });

            key = key - 1;

        }


        // key = await Storage.get("noti_id");

        // while (key >0 ) {

        //     message = await Storage.get(key);

        //     this.state.inMessage.push({name:message.title , content:message.body});

        //     key = key -1;            

        // }


    }

    async get_value(key) {

        value = await Storage.get(key, value);

    }

    render() {

        const { navigate } = this.props.navigation;

        return (

            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.banner} >
                    <Image style={styles.logoImage}
                        source={require('../../assets/img/logo.png')} />
                    <Text style={styles.topTitle}>RESCUER</Text>
                    <Button style={styles.logoImage} transparent onPress={() => navigate("DrawerOpen")} >
                        <Icon name="md-menu" style={{ fontSize: width / 8, color: 'black' }} />
                    </Button>
                </TouchableOpacity>
                <UserTitle username={this.state.username} />
                <View style={styles.titleView}>
                    <Text style={styles.titleText}>Message</Text>
                </View>
                <View style={styles.bodyView}>
                    <View style={styles.leftView}>
                        <TouchableOpacity
                            style={this.state.newMessageButton}
                            onPress={() => this.newMessage()}>
                            <Text style={styles.buttonText}>
                                New Message
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={this.state.inboxButton}
                            onPress={() => this.inbox()}>
                            <Text style={styles.buttonText}>
                                Inbox
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={this.state.sentButton}
                            onPress={() => this.sent()}>
                            <Text style={styles.buttonText}>
                                Sent
                                </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.rightView}>
                        <View style={this.state.newMessageView}>
                            <View style={styles.topView}>
                                <Text style={styles.topText}>
                                    New Message
                                    </Text>
                            </View>
                            <View style={styles.sbodyView}>
                                <View style={styles.textAreaView}>
                                    <ScrollView>
                                        <KeyboardAvoidingView behavior="padding">
                                            <TextInput onChangeText={this.handelMessage} style={{ fontSize: 20, height: 500, backgroundColor: '#aaaaaa55' }} multiline={true} numberOfLines={20} />
                                        </KeyboardAvoidingView>
                                    </ScrollView>
                                </View>
                                <View style={styles.bottomView}>
                                    <View style={styles.imageButtonView}>
                                        <TouchableHighlight onPress={() => { this.fileButton() }}>
                                            <Image style={{ marginRight: 20, width: 30, height: 30 }} source={require('../../assets/img/fileButton.png')} />
                                        </TouchableHighlight>
                                        <TouchableHighlight onPress={() => { this.cameraButton() }}>
                                            <Image style={{ marginRight: 20, width: 30, height: 30 }} source={require('../../assets/img/cameraButton.png')} />
                                        </TouchableHighlight>
                                    </View>
                                    <View style={styles.imageButtonView}>
                                        <TouchableHighlight onPress={() => { this.sendButton() }}>
                                            <Image style={{ marginLeft: 20, width: 30, height: 30 }} source={require('../../assets/img/sendButton.png')} />
                                        </TouchableHighlight>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={this.state.inboxView}>
                            <View style={styles.topView}>
                                <Text style={styles.topText}>
                                    Inbox Messages
                                    </Text>
                            </View>
                            <View style={styles.sbodyView}>
                                <ScrollView>
                                    {
                                        this.state.inMessage.map((item, index) => (
                                            <TouchableHighlight key={item.name} onPress={() => { this.viewInMessage(item.content) }}>
                                                <View style={styles.item}>
                                                    <View style={{ width: '20%' }}>
                                                        <Image style={{ width: 40, height: 40 }} source={require('../../assets/img/down.png')} />
                                                    </View>
                                                    <View style={{ width: '70%' }}>
                                                        <Text style={{ fontSize: 20 }}>{item.name}</Text>
                                                        <Text>{item.content}</Text>
                                                    </View>
                                                </View>
                                            </TouchableHighlight>
                                        ))
                                    }
                                </ScrollView>
                            </View>
                        </View>
                        <View style={this.state.sentView}>
                            <View style={styles.topView}>
                                <Text style={styles.topText}>
                                    Sent Messages
                                    </Text>
                            </View>
                            <View style={styles.sbodyView}>
                                <ScrollView>
                                    {
                                        this.state.outMessage.map((item, index) => (
                                            <TouchableHighlight key={item.name} onPress={() => { this.viewOutMessage(item.content) }}>
                                                <View style={styles.item}>
                                                    <View style={{ width: '20%' }}>
                                                        <Image style={{ width: 40, height: 40 }} source={require('../../assets/img/up.png')} />
                                                    </View>
                                                    <View style={{ width: '70%' }}>
                                                        <Text style={{ fontSize: 20 }}>{item.name}</Text>
                                                        <Text>{item.content}</Text>
                                                    </View>
                                                </View>
                                            </TouchableHighlight>
                                        ))
                                    }
                                </ScrollView>
                            </View>
                        </View>


                    </View>
                </View>

            </View>
           
        )
    }

}

const styles = StyleSheet.create({
    banner: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(250,0,0,1)',
        width: '100%',
        height: height / 8

    },

    logoImage: {
        alignItems: 'center',
        width: '20%',
        height: height / 10,
        resizeMode: 'contain',
    },

    topTitle: {
        width: '60%',
        textAlign: 'center',
        fontSize: width / 10,
        fontWeight: '400',
        color: '#fff',
    },

    container: {
        marginTop: 0,
    },

    titleView: {
        width: '100%',
        height: height/10,
        backgroundColor: 'gray',
    },
    titleText: {
        fontSize: width / 11,
        fontWeight: '300',
        padding: 10,
        marginLeft: 20,
    },
    bodyView: {
        width: '100%',
        height: height-height/4,
        backgroundColor: '#cfd2d6',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    leftView: {
        width: '40%',
        height: '100%',
    },
    rightView: {
        width: '60%',
        height: '100%',
    },

    buttonText: {
        color: 'white',
        fontSize: 15,
        fontWeight: '100',
        textAlign: 'center'
    },

    topView: {
        width: '100%',
        height: '10%',
        alignItems: 'center',
        borderBottomColor: 'black',
        borderBottomWidth: 2,
    },

    topText: {
        fontSize: 20,
        fontWeight: '100',
    },

    sbodyView: {
        width: '100%',
        height: '90%',
        backgroundColor: 'white',
        borderRadius: 5,
    },

    textAreaView: {
        width: '100%',
        height: '75%',
    },

    bottomView: {
        width: '100%',
        height: '15%',
        backgroundColor: '#cfd2d690',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },

    imageButtonView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },

    item: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        margin: 2,
        padding: 5,
        borderRadius: 10,
        backgroundColor: 'gray',
    }

})