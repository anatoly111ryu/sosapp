// @flow
import React, { Component } from "react";
import { View, Image, StyleSheet, KeyboardAvoidingView, ScrollView, TouchableOpacity, Dimensions, Text, TextInput } from "react-native";
import { Container, Button, Header, Left, Right, Body, Icon, Title, Spinner, Footer, FooterTab, } from "native-base";

import { Styles, Images, Field, NavigationHelpers, Avatar } from "../components";

import variables from "../../native-base-theme/variables/commonColor";

import api from '../Utils/api';
import helper from '../Utils/helper';

const { width, height } = Dimensions.get("window");
export default class Detail extends Component {

    constructor(props) {
        super(props)

        var ls = require('react-native-local-storage');
        ls.get('is_athenticated').then((is_athenticated) => {

            if (!is_athenticated) {

                this.props.navigation.navigate('SignIn')
            }

        });

        this.state = {

            isLoading : 0,
            persons: 1,
            buildings: 1,
            vehicles: 1,

            detail_name: this.props.navigation.state.params.detailName,
            latitude: '0',
            longitude: '0',
            detail: '',
            initialPosition: '',

        }
    }

    componentDidMount = () => {

        navigator.geolocation.getCurrentPosition(
            (position) => {
                var initialPosition = JSON.stringify(position);
                this.setState({ initialPosition: JSON.parse(initialPosition) });
            },
            (error) => alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );

    }


    sendSOS() {

        this.setState({isLoading : 1});
        
        var initial = this.state.initialPosition;

        var detail_name = this.props.navigation.state.params.detailName;

        var sub_name = this.props.navigation.state.params.sub_name;

        var disaster_type = this.props.navigation.state.params.disaster_type;

        if (this.props.navigation.state.params.disaster_type == "undefined") {

            disaster_type = "medical";

        }

        var persons = this.state.persons;

        var buildings = this.state.buildings;
        
        var vehicles = this.state.vehicles;
               
        var description = this.state.detail;

        let user_id;
        
        var ls = require('react-native-local-storage');

        
        ls.get('user_id').then((user_id) => {

            console.log(sub_name);
            
            let data = {
                'userid': user_id,
                'disastertype': disaster_type,
                'sub_name' : sub_name,
                'detail_name': detail_name,
                'description': description,
                'latitude': initial.coords.latitude,
                'longitude': initial.coords.longitude,
                'persons':persons,
                'buildings':buildings,
                'vehicles':vehicles
            }

            console.log(data);
            api.postApi('signal_mobile.php', data)
                .then((res) => {

                    if (res.status == '1') {

                        this.setState({isLoading : 0});

                        this.props.navigation.navigate("SOS");

                    } else {
                        alert(res.msg);
                    }
                })
                .catch((e) => {
                    console.error(e);
                    alert('Error1')
                })
        })

    }

    personsDecrease() {

        if (this.state.persons > 1) {

            this.setState({ persons: this.state.persons - 1 })
        }

    }

    buildingsDecrease() {

        if (this.state.buildings > 1) {

            this.setState({ buildings: this.state.buildings - 1 })
        }

    }

    vehiclesDecrease() {

        if (this.state.vehicles > 1) {

            this.setState({ vehicles: this.state.vehicles - 1 })
        }

    }

    render() {
        const { navigate } = this.props.navigation

        if(!this.state.persons){
            return;
        }
    
        return (
            <View style={styles.containerView}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.banner} >
                    <Image style={styles.logoImage}
                        source={require('../../assets/img/logo.png')} />
                    <Text style={styles.topTitle}>RESCUER</Text>
                    <Button style={styles.logoImage} transparent onPress={() => navigate("DrawerOpen")} >
                        <Icon name="md-menu" style={{ fontSize: width / 8, color: 'black' }} />
                    </Button>
                </TouchableOpacity>
                <KeyboardAvoidingView behavior="padding" style={styles.container}>

                    <View style={styles.leftView}>
                        <Button
                            onPress={() => navigate("Medical", { data: 'Medical' })}
                            style={styles.leftButton} >

                            <Icon name='ios-medkit' style={styles.Icon} />
                            <Text style={styles.tab_title}>Medical</Text>

                        </Button>

                        <Button
                            onPress={() => navigate("Disaster", { data: 'disaster' })}
                            style={styles.leftButton}
                        >
                            <Icon name='ios-bonfire' style={styles.Icon} />
                            <Text style={styles.tab_title}>Disaster</Text>
                        </Button>

                        <Button
                            onPress={() => navigate("Crash")}
                            style={styles.leftButton}
                        >
                            <Icon name='ios-car' style={styles.Icon} />
                            <Text style={styles.tab_title}>Crash</Text>
                        </Button>

                        <Button
                            onPress={() => navigate("Accident")}
                            style={styles.leftButton}
                        >
                            <Icon name="md-alert" style={styles.Icon} />
                            <Text style={styles.tab_title}>Accident</Text>
                        </Button>

                        <Button
                            onPress={() => navigate("Help")}
                            style={styles.leftButton}
                        >
                            <Icon name="ios-walk" style={styles.Icon} />
                            <Text style={styles.tab_title}>Help</Text>
                        </Button>
                    </View>
                    <View style={styles.rightView}>
                        <ScrollView style={{ flex: 1 },styles.itemContainer} >
                            <View style={styles.item}>
                                <Text style={styles.title}>{this.state.detail_name}</Text>
                            </View>

                            {this._personsText()}
                            {this._buildingsText()}
                            {this._vehiclesText()}
                            



                            <TextInput
                                value={this.state.detail}
                                style={styles.description}
                                multiline={true} numberOfLines={20}
                                onChangeText={(detail) => this.setState({ detail })} />                
                        </ScrollView >
                <Button primary full onPress={() => { this.sendSOS() }} style={{ height: height / 10 }}>
                    {this.state.isLoading ? <Spinner color="white" /> : <Text style={{ fontSize: height / 15, color: "white" }}>S O S</Text>}
                </Button>

            </View >              
                
            </KeyboardAvoidingView >
            </View >
        )
    }


    _personsText() {

        return (

            <View>
                <View style={{ margin: 5 }}>
                    <Text style={styles.title}>Endangered persons </Text>
                </View>

                <View style={{ flexDirection: 'row', margin: 5, alignSelf: "center" }}>
                    <TouchableOpacity onPress={() => { this.personsDecrease() }}>
                        <Image source={Images.minus} style={styles.image}
                            value={this.state.persons}
                            onClick={() => this.setState({ persons: this.state.persons + 1 })} />
                    </TouchableOpacity>

                    <View style={styles.textView}>
                        <Text style={styles.title, {color: 'black', fontSize:height/20, fontWeight: '800'}}>{this.state.persons}</Text>
                    </View>
                    <TouchableOpacity onPress={() => { this.setState({ persons: this.state.persons + 1 }) }}>
                        <Image source={Images.plus} style={styles.image}
                            value={this.state.persons}
                            onClick={() => this.setState({ persons: this.state.persons + 1 })} />
                    </TouchableOpacity>
                </View >
            </View>

        )

    }

    _buildingsText() {

        let detail = this.state.detail_name;
        let disaster_type = this.props.navigation.state.params.disaster_type;
        let sub_name = this.props.navigation.state.params.sub_name;
      
        if( detail == 'Building' || 
            sub_name == 'Vehicles' || 
            sub_name == 'Flooding' ||
            detail == 'Earthquake' ||
            detail == 'Landslide' ||
            detail == 'Storm and strong winds' || 
            disaster_type == 'Accident' 
          )
        {

            return (
                <View>
                    <View style={{ margin: 5 }}>
                        <Text style={styles.title}>Endangered buildings</Text>
                    </View>

                    <View style={{ flexDirection: 'row', margin: 5, alignSelf: "center" }}>
                        <TouchableOpacity onPress={() => { this.buildingsDecrease() }}>
                            <Image source={Images.minus} style={styles.image}
                                value={this.state.buildings}
                                onClick={() => this.setState({ buildings: this.state.buildings + 1 })} />
                        </TouchableOpacity>

                        <View style={styles.textView}>
                            <Text style={styles.title, {color: 'black', fontSize:height/20, fontWeight: '800'}}>
                                {this.state.buildings}
                            </Text>
                        </View>
                        <TouchableOpacity onPress={() => { this.setState({ buildings: this.state.buildings + 1 }) }}>
                            <Image source={Images.plus} style={styles.image}
                                value={this.state.buildings}
                                onClick={() => this.setState({ buildings: this.state.buildings + 1 })} />
                        </TouchableOpacity>
                    </View>
                </View>
            
            )
        }
    }

    _vehiclesText() {

        let detail = this.state.detail_name;
        let disaster_type = this.props.navigation.state.params.disaster_type;
        let sub_name = this.props.navigation.state.params.sub_name;        
        if( disaster_type == 'Crash' ||
            disaster_type == 'Accident'
            )
        {
            return (
                <View>
                    <View style={{ margin: 5 }}>
                        <Text style={styles.title}>Endangered Vehicles </Text>
                    </View>

                    <View style={{ flexDirection: 'row', margin: 5, alignSelf: "center" }}>
                        <TouchableOpacity onPress={() => { this.vehiclesDecrease() }}>
                            <Image source={Images.minus} style={styles.image}
                                value={this.state.vehicles}
                                onClick={() => this.setState({ vehicles: this.state.vehicles + 1 })} />
                        </TouchableOpacity>

                        <View style={styles.textView}>
                            <Text style={styles.title, {color: 'black', fontSize:height/20, fontWeight: '800'}}>
                                {this.state.vehicles}
                            </Text>
                    </View>
                    <TouchableOpacity onPress={() => { this.setState({ vehicles: this.state.vehicles + 1 }) }}>
                        <Image source={Images.plus} style={styles.image}
                            value={this.state.vehicles}
                            onClick={() => this.setState({ vehicles: this.state.vehicles + 1 })} />
                    </TouchableOpacity>
                </View>
            </View >   


            )            
        }

    }
}

const styles = StyleSheet.create({
    containerView: {
        width: width,
        height: height,
        flexDirection: 'column'
    },
    container: {
        width: width,
        height: height - height / 8,
        flexDirection: 'row'
    },
    leftView: {
        width: '30%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#000000AA',
    },
    rightView: {
        width: '70%',
        height: '100%',
    },

    leftButton: {
        height: "19.5%",
        width: "100%",
        flexDirection: 'column',
        backgroundColor: '#000000AA',
    },

    banner: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(250,0,0,1)',
        width: '100%',
        height: height / 8

    },

    logoImage: {
        alignItems: 'center',
        width: '20%',
        height: height / 10,
        resizeMode: 'contain',
    },

    topTitle: {
        width: '60%',
        textAlign: 'center',
        fontSize: width / 10,
        fontWeight: '400',
        color: '#fff',
    },

    image: {
        width: width / 7,
        height: height / 15,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    Icon: {
        fontSize: width / 6,
        color: 'white'
    },
    tab_title: {
        fontSize: width / 25,
        color: 'white'
    },

    item: {
        paddingHorizontal: width / 23.5,
        paddingVertical: 2,
        borderColor: '#fff',
        borderWidth: 5,
        borderRadius: 5,
        margin: 5,
    },
    itemContainer: {
        backgroundColor: 'rgba(14,14,14,10)',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#ddd',
    },

    title: {
        fontSize: width / 20,
        fontWeight: 'bold',
        color: 'white',
    },
    textView: {
        width: height / 7,
        height: height / 13,
        marginLeft: 20,
        marginRight: 20,
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 5 
    },
    description: {
        borderRadius: 5,
        height: height / 4,
        padding: 15,
        alignItems: "center",
        backgroundColor: 'white',
        fontSize: 20,
        justifyContent: 'center',
    }

});

